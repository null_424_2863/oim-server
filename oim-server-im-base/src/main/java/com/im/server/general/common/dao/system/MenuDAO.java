package com.im.server.general.common.dao.system;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.im.base.dao.BaseDAO;
import com.im.server.general.common.bean.system.Menu;
import com.onlyxiahui.query.hibernate.QueryWrapper;
import com.onlyxiahui.query.hibernate.util.QueryUtil;

/**
 * 
 * date 2018-07-04 17:01:47<br>
 * description
 * 
 * @author XiaHui<br>
 * @since
 */
@Repository
public class MenuDAO extends BaseDAO {

	String namespace = "menu";//Menu.class.getName();

	public Menu get(String id) {
		return this.readDAO.get(Menu.class, id);
	}

	public void delete(String id) {
		writeDAO.deleteById(Menu.class, id);
	}

	public int getMaxRankBySuperId(String superId) {
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.addParameter("superId", superId);
		Integer maxRank = this.queryUniqueResultByName(namespace + ".getMaxRankBySuperId", queryWrapper, Integer.class);
		return (null == maxRank) ? -1 : maxRank;
	}

	public int updateSuperId(String id, String superId) {
		QueryWrapper map = new QueryWrapper();
		map.put("id", id);
		map.put("superId", superId);
		int i = writeDAO.executeSQLByName(namespace + ".updateSuperId", map);
		return i;
	}

	public int updateRank(String id, int rank) {
		QueryWrapper map = new QueryWrapper();
		map.put("id", id);
		map.put("rank", rank);
		int i = writeDAO.executeSQLByName(namespace + ".updateRank", map);
		return i;
	}

	public List<Menu> queryList(QueryWrapper queryWrapper) {
		List<Menu> menuList = readDAO.queryListByName(namespace + ".queryList", queryWrapper, Menu.class);
		return menuList;
	}

	/**
	 * 获取所有菜单的实体信息
	 * 
	 * @return
	 */
	public List<Menu> getAllMenuList() {
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.put("orderBy", " order by rank ");
		List<Menu> menuList = readDAO.queryListByName(namespace + ".queryList", queryWrapper, Menu.class);
		return menuList;
	}

	public List<Menu> getMenuListBySuperId(String superId) {
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.put("superId", superId);
		queryWrapper.put("orderBy", " order by rank ");
		List<Menu> menuList = readDAO.queryListByName(namespace + ".queryList", queryWrapper, Menu.class);
		return menuList;
	}

	public int updateMapBySelective(Map<String, Object> map) {
		QueryWrapper qw = QueryUtil.getQueryWrapper(map);
		int i = writeDAO.executeSQLByName(namespace + ".updateSelective", qw);
		return i;
	}
}

package com.im.server.general.manage.core.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.im.server.general.common.bean.Group;
import com.im.server.general.manage.common.annotation.PermissionMapping;
import com.im.server.general.manage.core.service.GroupService;
import com.onlyxiahui.common.message.result.ResultMessage;
import com.onlyxiahui.general.annotation.parameter.Define;
import com.onlyxiahui.general.annotation.parameter.RequestParameter;
import com.onlyxiahui.im.message.data.PageData;
import com.onlyxiahui.im.message.data.query.GroupQuery;
import com.onlyxiahui.query.page.DefaultPage;

/**
 * 
 * date 2018-07-19 09:29:16<br>
 * description 群管理
 * @author XiaHui<br>
 * @since
 */
@Controller
@RequestMapping("/manage/core")
public class GroupController {
	@Resource
	GroupService groupService;

	@ResponseBody
	@RequestParameter
	@PermissionMapping(name = "群列表", key = "/manage/core/group/list", superKey = "core", type = PermissionMapping.Type.menu)
	@RequestMapping(method = RequestMethod.POST, value = "/group/list")
	public Object list(HttpServletRequest request,
			@Define("groupQuery") GroupQuery groupQuery,
			@Define("page") PageData page) {
		ResultMessage rm = new ResultMessage();
		try {
			DefaultPage defaultPage = new DefaultPage();
			defaultPage.setPageNumber(page.getPageNumber());
			defaultPage.setPageSize(page.getPageSize());
			List<Group> list = groupService.queryGroupList(groupQuery, defaultPage);
			rm.put("list", list);
			rm.put("page", defaultPage);
		} catch (Exception e) {
			e.printStackTrace();
			rm.addError("500", "系统异常");
		}
		return rm;
	}
}
